<%@ page import="bll.Helper" %>
<%@ page import="java.util.Map" %>
<%@ page import="model.Flight" %>
<%@ page import="java.util.List" %><%--
  Created by IntelliJ IDEA.
  User: istra
  Date: 11/20/2018
  Time: 10:32 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <style>
        * {
            box-sizing: border-box;
        }

        .myInput {
            width: 15%;
            font-size: 16px;
            padding: 12px 20px 12px 40px;
            border: 1px solid #ddd;
            margin-bottom: 12px;
            font-family: sans-serif;
        }

        #myTable {
            border-collapse: collapse;
            width: 50%;
            border: 1px solid #ddd;
            position:center;
            font-size: 18px;
            font-family: sans-serif;
        }

        #myTable th, #myTable td {
            text-align: left;
            padding: 12px;
            font-family: sans-serif;
        }

        #myTable tr {
            border-bottom: 1px solid #ddd;
            font-family: sans-serif;
        }

        #myTable tr.header, #myTable tr:hover {
            background-color: #ffb100;
            font-family: sans-serif;
        }
    </style>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

</head>
<body>
<nav class="navbar navbar-inverse">
    <div class="container-fluid">
        <div class="navbar-header">
            <a class="navbar-brand" href="#">Flights</a>
        </div>

    </div>
</nav>
<form action="" method="post">
    <input type="text" class="myInput" name="longitude" placeholder="Longitude..." title="Type in a name" style="right:100px">
    <input type="text" class="myInput" name="latitude" placeholder="Latitude..." title="Type in a name" style="right:100px">
</form>
<form action="ClientServlet" method = "get">
    <table id="myTable">
        <tr class="header">
            <th style="width:10%;">Flight number</th>
            <th style="width:10%;">Airplane Type</th>
            <th style="width:10%;">Departure city</th>
            <th style="width:10%;">Departure date/hour</th>
            <th style="width:10%;">Arrival city</th>
            <th style="width:10%;">Arrival date/hour</th>
        </tr>
        <tbody>
        <%
            String username=null;
            Cookie[] cookies = request.getCookies();

            if(cookies!=null){
                for(Cookie cookie : cookies){
                    if(cookie.getName().equals("ClientMode")){
                        username = cookie.getValue();
                    }
                }
            }
            if(username == null){
                response.sendRedirect("index.jsp");
            }

            Helper helper = new Helper();
            Map<Integer,String> cityMap = helper.createMap();
            List<Flight> flights = helper.getAllFlights();
            for(Flight flight: flights){

        %>

        <tr>
            <td><%=flight.getId() %></td>
            <td><%=flight.getAirplane_type()%> </td>
            <td><%=cityMap.get(flight.getDeparture_city())%></td>
            <td><%=flight.getDeparture_date()%></td>
            <td><%=cityMap.get(flight.getArrival_city_id())%></td>
            <td><%=flight.getArrival_date()%></td>
        </tr>
        <%
            }
        %>
        </tbody>
    </table>
</form>
<script>
    function myFunction() {
        var input, filter, table, tr, td, i;
        input = document.getElementById("myInput");
        filter = input.value.toUpperCase();
        table = document.getElementById("myTable");
        tr = table.getElementsByTagName("tr");
        for (i = 0; i < tr.length; i++) {
            td = tr[i].getElementsByTagName("td")[0];
            if (td) {
                if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
                    tr[i].style.display = "";
                } else {
                    tr[i].style.display = "none";
                }
            }
        }
    }
</script>

</body>
</html>
