
import dataAccess.AdminDAO;
import dataAccess.CityDao;
import dataAccess.ClientDAO;
import dataAccess.FlightDAO;
import model.Administrator;
import model.City;
import model.Client;
import model.Flight;

import java.util.List;

public class Main {

    public static void main(String[] args){

        AdminDAO adminDAO = new AdminDAO();
        System.out.println(adminDAO.findByUsername("admiewn"));
        CityDao cityDao = new CityDao();

        List<City> cities = cityDao.getAll();

        FlightDAO flightDao = new FlightDAO();
        List<Flight> flights = flightDao.getAll();

        for(Flight flight: flights){
            System.out.println(flight.getAirplane_type() + " " + flight.getId());
        }
        for(City city : cities){
            System.out.println(city.getCity_name());
        }
//       ClientDAO clientDAO = new ClientDAO();
//       List<Client> clients = clientDAO.getAll();
//       for(Client client:clients){
//           System.out.println(client.toString());
//       }
    }
}
