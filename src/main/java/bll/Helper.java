package bll;

import dataAccess.AdminDAO;
import dataAccess.CityDao;
import dataAccess.FlightDAO;
import model.City;
import model.Flight;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Helper {

    private AdminDAO adminDAO = new AdminDAO();
    private CityDao cityDao = new CityDao();
    private FlightDAO flightDAO = new FlightDAO();
    private Map<Integer,String> cityMap;

    public Map<Integer, String> createMap(){
        Map<Integer, String> cityMap = new HashMap<>();
        List<City> cityList = cityDao.getAll();

        for(City city : cityList){
            cityMap.put(city.getId(),city.getCity_name());
        }
        return cityMap;
    }

    public List<Flight> getAllFlights(){
        List<Flight> flights = flightDAO.getAll();
        return flights;
    }
}
