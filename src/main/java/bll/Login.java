package bll;

import dataAccess.AdminDAO;
import dataAccess.ClientDAO;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name ="Welcome", urlPatterns = "/Welcome")
public class Login extends HttpServlet {

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String username = request.getParameter("username");
        String password = request.getParameter("password");
        AdminDAO admin = new AdminDAO();
        ClientDAO client = new ClientDAO();

        if(admin.findByCredentials(username, password)){
            Cookie adminCookie = new Cookie("AdminMode",username);
                System.out.println("Logged in with admin: "+username + " "+password);
                adminCookie.setMaxAge(30*60);
                response.addCookie(adminCookie);
                response.sendRedirect("/ServletAdmin");

        } else if(client.findByUsername(username)){
            Cookie clientCookie = new Cookie("ClientMode",username);
            System.out.println("Logged in with user: "+username);
            clientCookie.setMaxAge(30*60);
            response.addCookie(clientCookie);
            response.sendRedirect("/ClientServlet");
        }

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
