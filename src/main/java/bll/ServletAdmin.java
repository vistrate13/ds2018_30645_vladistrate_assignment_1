package bll;

import dataAccess.AdminDAO;
import dataAccess.FlightDAO;
import model.Administrator;
import model.Flight;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

@WebServlet(name ="ServletAdmin", urlPatterns = "/ServletAdmin")
public class ServletAdmin extends HttpServlet {
    AdminDAO adminDao;

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
//        FlightDAO flightDAO = new FlightDAO();
//        List<Flight> flights = flightDAO.getAll();
//
//        request.setAttribute("flights",flights);
        request.getRequestDispatcher("/adminMode1.jsp").forward(request, response);
    }
}
