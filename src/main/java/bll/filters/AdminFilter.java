package bll.filters;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebFilter(filterName = "AdminFilter",servletNames = "ServletAdmin", urlPatterns = "/ServletAdmin")
public class AdminFilter implements Filter {

    private ServletContext servletContext;
    public void destroy() {
    }

    public void doFilter(ServletRequest req, ServletResponse resp, FilterChain chain) throws ServletException, IOException {
        HttpServletRequest request = (HttpServletRequest) req;
        HttpServletResponse response = (HttpServletResponse) resp;

        Cookie[] cookies = request.getCookies();
        Cookie login = null;
        for(Cookie cookie: cookies){
            if(cookie.getName().equals("AdminMode")){
                login = cookie;
            }
        }

        if(login == null){
            response.sendRedirect(request.getContextPath() + "/index.jsp");

        } else{
            chain.doFilter(req, resp);
        }

    }

    public void init(FilterConfig config) throws ServletException {
        this.servletContext= config.getServletContext();
    }

}
