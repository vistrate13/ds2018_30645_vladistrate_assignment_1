package dataAccess;

import model.Administrator;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import java.util.List;

public class AdminDAO {
    private EntityManager entityManager;
    private EntityManagerFactory entityManagerFactory;

    QueryConstruct queryConstruct = new QueryConstruct();

    public List<Administrator> getAll(){
        List<Administrator> administrators =  queryConstruct.createFactory("administrator.getAll").getResultList();
        queryConstruct.close();
        return administrators;
    }

    public boolean findByUsername(String username){

        entityManagerFactory = Persistence.createEntityManagerFactory("org.hibernate.tutorial.jpa");
        entityManager = entityManagerFactory.createEntityManager();
        Query query = entityManager.createNamedQuery("administrator.findByUsername");
        query.setParameter("username",username);
        List<Administrator> admin = query.getResultList();
        if (admin.isEmpty()) {
            return false;
        }

        entityManagerFactory.close();
        return true;
    }

    public boolean findByCredentials(String username, String password){

        entityManagerFactory = Persistence.createEntityManagerFactory("org.hibernate.tutorial.jpa");
        entityManager = entityManagerFactory.createEntityManager();
        Query query = entityManager.createNamedQuery("administrator.findByCredentials");
        query.setParameter("username",username);
        query.setParameter("password",password);
        List<Administrator> admin = query.getResultList();
        if (admin.isEmpty()) {
            return false;
        }

        entityManagerFactory.close();
        return true;
    }


}
