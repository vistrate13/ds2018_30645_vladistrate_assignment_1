package dataAccess;

import model.City;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import java.util.List;

public class CityDao {

    private EntityManager entityManager;
    private EntityManagerFactory entityManagerFactory;

    QueryConstruct queryConstruct = new QueryConstruct();

    public List<City> getAll(){
        List<City> administrators =  queryConstruct.createFactory("city.getAll").getResultList();
        queryConstruct.close();
        return administrators;
    }
}
