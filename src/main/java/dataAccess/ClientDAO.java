package dataAccess;

import model.Administrator;
import model.Client;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import java.util.List;

public class ClientDAO {

    QueryConstruct queryConstruct = new QueryConstruct();
    private EntityManager entityManager;
    private EntityManagerFactory entityManagerFactory;

    public List<Client> getAll(){
        List<Client> clients = queryConstruct.createFactory("client.getAll")
                .getResultList();
        queryConstruct.close();
        return clients;
    }

    public boolean findByUsername(String username){

        entityManagerFactory = Persistence.createEntityManagerFactory("org.hibernate.tutorial.jpa");
        entityManager = entityManagerFactory.createEntityManager();
        Query query = entityManager.createNamedQuery("client.findByUsername");
        query.setParameter("username",username);
        List<Client> admin = query.getResultList();
        if (admin.isEmpty()) {
            System.out.println("Username gresit admin");
            return false;
        }

        entityManagerFactory.close();
        return true;
    }
}
