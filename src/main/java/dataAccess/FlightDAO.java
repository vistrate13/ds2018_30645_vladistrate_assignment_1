package dataAccess;

import model.City;
import model.Flight;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import java.util.List;

public class FlightDAO {

    private EntityManager entityManager;
    private EntityManagerFactory entityManagerFactory;

    QueryConstruct queryConstruct = new QueryConstruct();

    public List<Flight> getAll(){
        List<Flight> administrators =  queryConstruct.createFactory("flight.getAll").getResultList();
        queryConstruct.close();
        return administrators;
    }

    public void deleteById(Integer flightId){
        entityManagerFactory = Persistence.createEntityManagerFactory("org.hibernate.tutorial.jpa");
        entityManager = entityManagerFactory.createEntityManager();
        entityManager.getTransaction().begin();
        Query query = entityManager.createNamedQuery("flight.deleteById");
        query.setParameter("id",flightId);
        query.executeUpdate();
        entityManager.getTransaction().commit();
        entityManagerFactory.close();
    }
}
