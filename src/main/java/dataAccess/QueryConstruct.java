package dataAccess;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

public class QueryConstruct {

    private EntityManager entityManager;
    private EntityManagerFactory entityManagerFactory;

    public Query createFactory(String queryName){
        entityManagerFactory = Persistence.createEntityManagerFactory("org.hibernate.tutorial.jpa");
        entityManager = entityManagerFactory.createEntityManager();
        Query query = entityManager.createNamedQuery(queryName);
        return query;

    }

    public void close(){
        this.entityManagerFactory.close();
    }
}
