package model;

import javax.persistence.*;

@Entity
@Table(name ="administrator")
@NamedQueries({
        @NamedQuery(name="administrator.getAll", query = "select c from Administrator c"),
        @NamedQuery(name="administrator.findByUsername", query = "select c from Administrator c where c.username=:username"),
        @NamedQuery(name="administrator.findByCredentials", query = "select c from Administrator c where c.username=:username and c.password=:password")

})

public class Administrator {

    @Id
    @Column(name="id")
    private int id;

    @Column(name="username")
    private String username;

    @Column(name="password_admin")
    private String password;

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "Id:" +id +"\nName: " + username +"\nPassword: " + password + "\n";
    }
}
