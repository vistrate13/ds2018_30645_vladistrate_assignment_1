package model;

import javax.persistence.*;

@Entity
@Table(name = "city")
@NamedQueries({
        @NamedQuery(name = "city.getAll" , query = "select c from City c")
})
public class City {

    @Id
    @Column(name = "id")
    private Integer id;

    @Column(name = "city_name")
    private String city_name;

    @Column(name = "longitude")
    private String longitude;

    @Column(name = "latitude")
    private String latitude;

    public void setId(int id) {
        this.id = id;
    }

    public void setCity_name(String city_name) {
        this.city_name = city_name;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public int getId() {
        return id;
    }

    public String getCity_name() {
        return city_name;
    }

    public String getLatitude() {
        return latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    @Override
    public String toString() {
        return super.toString();
    }
}
