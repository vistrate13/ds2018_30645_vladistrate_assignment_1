package model;

import javax.persistence.*;

@Entity
@Table(name = "clients")
@NamedQueries({
        @NamedQuery(name = "client.getAll" , query = "select c from Client c"),
        @NamedQuery(name="client.findByUsername", query = "select c from Client c where c.username=:username")
})

public class Client {

    @Id
    @Column(name = "id")
    private int id;

    @Column(name="username")
    private String username;

    @Column(name="password_user")
    private String password;

    public void setPassword(String password) {
        this.password = password;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPassword() {
        return password;
    }

    public String getUsername() {
        return username;
    }

    public int getId() {
        return id;
    }

    @Override
    public String toString() {
        return "Username: "+ username +"\nPassword: " + password;
    }
}
