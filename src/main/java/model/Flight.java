package model;

import javax.persistence.*;

@Entity
@Table(name = "flight")
@NamedQueries({
        @NamedQuery(name="flight.getAll", query = "select c from Flight c"),
        @NamedQuery(name="flight.deleteById", query = "delete from Flight as c where c.id=:id")
})
public class Flight {

    @Id
    @Column(name = "id")
    private Integer id;

    @Column(name = "airplane_type")
    private String airplane_type;

    @Column(name = "departure_city_id")
    private Integer departure_city;

    @Column(name = "departure_date")
    private String departure_date;

    @Column(name = "arrival_city_id")
    private Integer arrival_city_id;

    @Column(name = "arrival_date")
    private String arrival_date;

    public int getId() {
        return id;
    }

    public String getAirplane_type() {
        return airplane_type;
    }

    public Integer getArrival_city_id() {
        return arrival_city_id;
    }

    public String getArrival_date() {
        return arrival_date;
    }

    public Integer getDeparture_city() {
        return departure_city;
    }

    public String getDeparture_date() {
        return departure_date;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setAirplane_type(String airplane_type) {
        this.airplane_type = airplane_type;
    }

    public void setArrival_city_id(Integer arrival_city_id) {
        this.arrival_city_id = arrival_city_id;
    }

    public void setArrival_date(String arrival_date) {
        this.arrival_date = arrival_date;
    }

    public void setDeparture_city(Integer departure_city) {
        this.departure_city = departure_city;
    }

    public void setDeparture_date(String departure_date) {
        this.departure_date = departure_date;
    }
}
