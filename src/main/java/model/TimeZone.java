package model;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class TimeZone {

    @XmlElement(name = "localtime")
    private String localTime;

    public String getLocalTime() {
        return localTime;
    }
}
