<%@ page import="model.Flight" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.Map" %>
<%@ page import="bll.Helper" %>
<%@ page import="dataAccess.FlightDAO" %>
<%@ page import="java.util.ArrayList" %><%--
  Created by IntelliJ IDEA.
  User: istra
  Date: 11/7/2018
  Time: 11:26 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <style>
        * {
            box-sizing: border-box;
        }

        #myInput {
            width: 30%;
            font-size: 16px;
            padding: 12px 20px 12px 40px;
            border: 1px solid #ddd;
            margin-bottom: 12px;
            font-family: sans-serif;
        }

        #myTable {
            border-collapse: collapse;
            width: 70%;
            border: 1px solid #ddd;
            position:center;
            font-size: 18px;
            font-family: sans-serif;
        }

        #myTable th, #myTable td {
            text-align: left;
            padding: 12px;
            font-family: sans-serif;
        }

        #myTable tr {
            border-bottom: 1px solid #ddd;
            font-family: sans-serif;
        }

        #myTable tr.header, #myTable tr:hover {
            background-color: #8cdbff;
            font-family: sans-serif;
        }

        .button{
            background-color: #01cd04;
            border: none;
            color:white;
            padding: 5px 18px;
            text-align: center;
            text-decoration:none;
            display:inline-block;
            font-size:15px;
            margin:4px 2px;
            cursor: pointer;
            border-radius: 10px;
        }
        .button2{
            background-color: #d20000;
        }
    </style>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

</head>
<body>
<nav class="navbar navbar-inverse">
    <div class="container-fluid">
        <div class="navbar-header">
            <a class="navbar-brand" href="#">Flights</a>
        </div>

    </div>
</nav>
<input type="text" id="myInput" onkeyup="myFunction()" placeholder="Search for flight number.." title="Type in a name" style="right:100px">

<form action="ServletAdmin" method = "get">
<table id="myTable">
    <tr class="header">
        <th style="width:10%;">Flight number</th>
        <th style="width:10%;">Airplane Type</th>
        <th style="width:10%;">Departure city</th>
        <th style="width:10%;">Departure date/hour</th>
        <th style="width:10%;">Arrival city</th>
        <th style="width:10%;">Arrival date/hour</th>
        <th style="width:10%;"></th>
        <th style="width:10%;"></th>
    </tr>
    <tbody>
    <%
        String username=null;
        Cookie[] cookies = request.getCookies();

        if(cookies!=null){
            for(Cookie cookie : cookies){
                if(cookie.getName().equals("AdminMode")){
                    username = cookie.getValue();
                }
            }
        }
        if(username == null){
            response.sendRedirect("index.jsp");
        }
        Helper helper = new Helper();
        Map<Integer,String> cityMap = helper.createMap();
        List<Flight> flights = helper.getAllFlights();
        Integer id = null;
        for(Flight flight: flights){

    %>
    <tr>
        <td><%=flight.getId() %></td>
        <td><%=flight.getAirplane_type()%> </td>
        <td><%=cityMap.get(flight.getDeparture_city())%></td>
        <td><%=flight.getDeparture_date()%></td>
        <td><%=cityMap.get(flight.getArrival_city_id())%></td>
        <td><%=flight.getArrival_date()%></td>
        <td><button class="button" type="submit">Update</button></td>
        <td><button class="button button2">
            Delete
            <%
                id = flight.getId();
            %>
        </button></td>
    </tr>
    <%
        }
    %>

    </tbody>
</table>
</form>

<script>
    function myFunction() {
        var input, filter, table, tr, td, i;
        input = document.getElementById("myInput");
        filter = input.value.toUpperCase();
        table = document.getElementById("myTable");
        tr = table.getElementsByTagName("tr");
        for (i = 0; i < tr.length; i++) {
            td = tr[i].getElementsByTagName("td")[0];
            if (td) {
                if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
                    tr[i].style.display = "";
                } else {
                    tr[i].style.display = "none";
                }
            }
        }
    }
</script>

</body>
</html>
